import jwt from 'jsonwebtoken';

const loginTokenKey = process.env.REACT_APP_LOGIN_TOKEN_KEY || 'capimpact_accesstoken';

class AuthService {
  getUser() {
    if (this.isAuthenticated()) {
      let token = this.getToken();
      const decoded = jwt.decode(token);
      return decoded;
    }
    return null;
  }

  isAuthenticated() {
    const token = this.getToken();
    return !!(token && !this.isTokenExpired(token));
  }

  onLogin(response, context = {}) {
    const { history, location } = context;
    let { from } = location.state || { from: { pathname: '/' } };
    let token = response.access_token;
    if (token) {
      this.setToken(token);
      history.replace(from);
    }
  }

  logout(context = {}) {
    const { history } = context;
    this.destroyToken();
    history.replace('/login');
  }

  getToken() {
    return localStorage.getItem(loginTokenKey);
  }

  setToken(token) {
    return !this.isTokenExpired(token) && localStorage.setItem(loginTokenKey, token);
  }

  destroyToken() {
    return localStorage.removeItem(loginTokenKey);
  }

  isTokenExpired = token => {
    token = token || this.getToken();
    if (!token) {
      return true;
    }
    const decoded = jwt.decode(token);
    let expiryAt = new Date(decoded.exp * 1000);
    let now = new Date();
    return expiryAt <= now;
  };
}

export default new AuthService();
