import React from "react";
import VideoHeader from "components/VideoHeader";
import {
  Input,
  Col,
  Form,
  FormGroup,
  FormFeedback,
  Button,
  Jumbotron,
  Container,
} from "reactstrap";
import classnames from "classnames";
import Webrtc from "./Webrtc";

class Lobby extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      displayName: "",
      displayNameError: false,
      roomId: "",
    };

    this.handleChangeDisplayName = this.handleChangeDisplayName.bind(this);
    this.handleChangeRoomId = this.handleChangeRoomId.bind(this);
    this.startMeet = this.startMeet.bind(this);
  }

  handleChangeDisplayName(event) {
    this.setState({
      displayName: event.target.value,
      displayNameError: false,
    });
  }

  handleChangeRoomId(event) {
    this.setState({
      roomId: event.target.value.trim(),
    });
  }

  startMeet(event) {
    event.preventDefault();

    if (this.state.roomId !== "" && this.state.displayName.trim() !== "") {
      this.props.history.push(
        `${this.props.match.url}meet/${
          this.state.roomId
        }/${this.state.displayName.trim()}`
      );
    } else if (this.state.displayName.trim() !== "") {
      const roomId = Webrtc.createUUID;
      this.props.history.push(
        `${this.props.match.url}meet/${roomId}/${this.state.displayName.trim()}`
      );
    } else {
      this.setState({
        displayNameError: true,
      });
    }
  }

  render() {
    return (
      <div className="lobby-room">
        <VideoHeader></VideoHeader>
        <Jumbotron fluid>
          <Container fluid>
            <h1 className="title display-2">SuMeet Lobby Room</h1>
            <p className="lead">
              Please, input your Display Name and start a free Meeting. If you
              have the Room ID you can join a running meet.
            </p>
          </Container>
        </Jumbotron>
        <div className="actions container">
          <Form className="form-lobby" onSubmit={this.startMeet}>
            <FormGroup row>
              <Col xs="12">
                <Input
                  placeholder="Display Name"
                  bsSize="lg"
                  className={classnames("display-name", {
                    "is-invalid": this.state.displayNameError,
                  })}
                  onChange={this.handleChangeDisplayName}
                  value={this.state.displayName}
                />
                {this.state.displayNameError ? (
                  <FormFeedback>Display Name is required</FormFeedback>
                ) : null}
              </Col>
            </FormGroup>
            <FormGroup row>
              <Col xs="12">
                <Input
                  placeholder="Room ID (Optional)"
                  bsSize="lg"
                  className="room-id"
                  onChange={this.handleChangeRoomId}
                  value={this.state.roomId}
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Col xs="12">
                <Button color="primary" size="lg" block type="submit">
                  Fire
                </Button>
              </Col>
            </FormGroup>
          </Form>
        </div>
      </div>
    );
  }
}

export default Lobby;
