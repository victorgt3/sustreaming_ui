import React from "react";
import renderer from "react-test-renderer";
import Lobby from "./Lobby";

it("Lobby renders correctly", () => {
  const tree = renderer.create(<Lobby></Lobby>).toJSON();
  expect(tree).toMatchSnapshot();
});
