import React from "react";
import renderer from "react-test-renderer";
import LobbyRoom from "./LobbyRoom";

it("Lobby Room renders correctly", () => {
  const match = { params: { roomId: "uniqueIdTest" } };
  const tree = renderer.create(<LobbyRoom match={match}></LobbyRoom>).toJSON();
  expect(tree).toMatchSnapshot();
});
