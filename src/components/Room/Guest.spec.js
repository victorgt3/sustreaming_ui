import React from "react";
import renderer from "react-test-renderer";
import Guest from "./Guest";

it("Lobby renders correctly", () => {
  const stream = {};
  const tree = renderer
    .create(
      <Guest
        videoSource={stream}
        id="guestIdTest"
        muted={true}
        controls={false}
        autoPlay={true}
      ></Guest>
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
