import { createCache } from 'react-fetching-library';

const cache = createCache(
  action => action.method === 'GET',
  response => new Date().getTime() - response.timestamp < 10 * 1000
);

export default cache;
