import qs from "qs";

/* Auth */
export const loginAction = ({ data }) => ({
  method: "POST",
  endpoint: "/auth/login",
  body: qs.stringify(data),
  headers: {
    "Content-Type": "application/x-www-form-urlencoded",
  },
});
