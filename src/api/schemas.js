import { object, string, number, date, array, lazy } from "yup";

export const loginSchema = object({
  email: string().required().email(),
  password: string().required(),
});
