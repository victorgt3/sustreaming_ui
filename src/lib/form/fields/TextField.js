import React from "react";
import { useField } from "formik";
import { FormGroup, Input, Label, FormFeedback, FormText } from "reactstrap";

const TextField = ({
  label,
  helperText,
  FormGroupProps = {},
  onChange,
  ...props
}) => {
  const [field, meta, fieldHelper] = useField(props);
  const handleChangeField = event => {
    fieldHelper.setValue(event.target.value);
    if (onChange !== undefined) {
      onChange(event);
    }
  };

  // Set value proproperty if this property is set and it hasn't fired the onchange event.
  if (
    field.value === undefined &&
    props.value !== undefined &&
    props.value !== null &&
    props.value !== "" &&
    meta.touched === false &&
    meta.error !== undefined
  ) {
    fieldHelper.setValue(props.value);
  }

  return (
    <FormGroup {...FormGroupProps}>
      {label && <Label for={props.id || props.name}>{label}</Label>}
      <Input
        {...field}
        {...props}
        onChange={handleChangeField}
        invalid={meta.touched && meta.error !== undefined}
      />
      {meta.touched && meta.error ? (
        <FormFeedback>{meta.error}</FormFeedback>
      ) : null}
      {helperText && <FormText>{helperText}</FormText>}
    </FormGroup>
  );
};

export default TextField;
