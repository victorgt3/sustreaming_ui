import React from 'react';
import { useField, useFormikContext } from 'formik';
import Select from 'react-select';
import { FormGroup, Label, FormFeedback, FormText } from 'reactstrap';

const SelectField = ({
  label,
  helperText,
  options,
  FormGroupProps = {},
  handleChange,
  ...props
}) => {
  const { setFieldValue } = useFormikContext();
  const [field, meta] = useField(props);

  return (
    <FormGroup {...FormGroupProps}>
      {label && <Label for={props.id || props.name}>{label}</Label>}
      <Select
        {...props}
        value={options.find(option => option.value === field.value)}
        onChange={value => {
          if (handleChange) {
            return handleChange({ field, meta, value: value.value });
          }
          return setFieldValue(field.name, value.value);
        }}
        options={options}
      />
      {meta.error ? <FormFeedback style={{ display: 'block' }}>{meta.error}</FormFeedback> : null}
      {helperText && <FormText>{helperText}</FormText>}
    </FormGroup>
  );
};

export default SelectField;
