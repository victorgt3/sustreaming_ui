import React from "react";
import { CustomInput, FormText } from "reactstrap";

const CheckboxField = ({ helperText, ...props }) => {
  const propreties = { ...props, type: "checkbox" };

  return (
    <React.Fragment>
      <CustomInput {...propreties} />
      {helperText && <FormText>{helperText}</FormText>}
    </React.Fragment>
  );
};

export default CheckboxField;
